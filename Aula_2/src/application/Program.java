package application;

import java.util.InputMismatchException;
import java.util.Scanner;

/*
 * @Autor: 2017200563	
 * 
 * 
 * 
 * */

public class Program {

	public static void main(String[] args) {
		
		try {
			//Declaracao do leitor de teclado 
			Scanner sc = new Scanner(System.in);
			
			//Pegando a primeira nota
			System.out.print("Digite a primeira nota: ");
			
			//Lendo oque ta escrito
			double av1 = sc.nextDouble();
			
			System.out.print("\nDigite a segunda nota: ");
			
			double av2 = sc.nextDouble();
			
			System.out.print("\nDigite a terceira nota: ");
			
			double av3 = sc.nextDouble();
			
			//Processamento
			
			double media = (av1+av2+av3)/3;
			
			if (media >= 7) {
				System.out.println("Aluno Aprovado com media: " + media);
			}else {
				System.out.println("Aluno Reprovado com media: " + media);
			} 
			
			//Saida
			//System.out.println("Media: " + media);
			
		
			
		} catch (InputMismatchException e) {
			System.out.println("Apenas numeros sao validos");
		}
	}
}